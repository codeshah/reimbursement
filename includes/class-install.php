<?php 
namespace WeDevs\ERP\Accounting\Reimbursement;

class install {
	/**
    
    itializes the class
     *
     * Checks for an existing instance
     * and if it doesn't find one, creates it.
     */
    public static function init() {
        static $instance = false;

        if ( ! $instance ) {
            $instance = new self();
        }

        return $instance;
    }

    /**
     * Reimbursement constructor
     */
    function __construct() {
    	$this->create_account();
    }

    /**
     * Create account for reimbursement
     *
     * @since 0.1
     * 
     * @return void
     */
    function create_account() {


    }
}

\WeDevs\ERP\Accounting\Reimbursement\install::init(); 


