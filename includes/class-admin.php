<?php
namespace WeDevs\ERP\Accounting\Reimbursement;

class Admin {
	/**
     *itializes the class
     *
     * Checks for an existing instance
     * and if it doesn't find one, creates it.
     */
    public static function init() {
        static $instance = false;

        if ( ! $instance ) {
            $instance = new self();
        }

        return $instance;
    }

    /**
     * Constructor for the class
     *
     * Sets up all the appropriate hooks and actions
     */
    public function __construct() {
        $this->actions();
        $this->filters();
    }

    /**
     * All actions are doing here
     *
     * @since  0.1
     *
     * @return  void
     */
    function actions() {
        add_action( 'admin_menu', [$this, 'menu'], 12 );
        //if ( current_user_can( 'erp_ac_manager' ) ) {
            add_action( 'load-accounting_page_erp-accounting-reimbursement', 'erp_ac_reimbur_balk_action' );
        //} else {
            add_action( 'load-hr-management_page_erp-accounting-reimbursement', 'erp_ac_reimbur_balk_action' );
        //}
        add_action( 'wp_ajax_erp-ac-reimbur-trns-row-status', 'erp_ac_reimbur_ajax_handel_trn_update' );
        add_action( 'erp_ac_new_transaction_reimbur', 'erp_ac_reibur_after_new_trans', 10, 3 );
    }

    /**
     * All filters are doing here
     *
     * @since  0.1
     *
     * @return  void
     */
    function filters() {
        add_filter( 'erp_ac_redirect_after_transaction', 'erp_reimburs_redirect', 10, 3 );
        add_filter( 'erp_ac_trans_status', 'erp_ac_reimbur_trans_status', 10, 2 );
        add_filter( 'erp_ac_register_type', 'erp_ac_reimbur_register_type' );
        add_filter( 'erp_ac_partial_types', 'erp_ac_reimbur_partial_types', 10, 2 );
        add_filter( 'erp_ac_is_due_trans', 'erp_ac_reimbur_is_due_trans', 10, 2 );
        add_filter( 'erp_ac_form_types', 'erp_ac_reimbursement_form_types', 10, 2 );
        add_filter( 'erp_ac_single_partial_payment_url', 'erp_ac_reimbur_single_partial_payment_url', 10, 2 );
        add_filter( 'erp_ac_trial_balance_where', 'erp_ac_reimbur_trial_balance_where' );
        add_filter( 'erp_ac_trial_balance_join', 'erp_ac_reimbure_trial_balance_join' );
    }

    /**
     * Constructor for the class
     *
     * Sets up all the appropriate hooks and actions
     */
    function menu() {

        if ( current_user_can( 'erp_ac_manager' ) ) {
            $reimbursement = add_submenu_page( 'erp-accounting', __( 'Reimbursement', 'reimbursement' ), __( 'Reimbursement', 'erp-reimburs' ), 'erp_ac_manager', 'erp-accounting-reimbursement', array( $this, 'reimursement' ) );
        } else {
            $reimbursement = add_submenu_page( 'erp-hr', __( 'Reimbursement', 'erp' ), __( 'Reimbursement', 'erp' ), 'read', 'erp-accounting-reimbursement', array( $this, 'reimursement' ) );
        }

        add_action( 'admin_print_styles-' . $reimbursement, array( $this, 'reimbursement_script' ) );
    }

    /**
     * Load reimbursement scripts
     *
     * @since 0.1
     *
     * @return void
     */
    public function reimbursement_script() {

        $chart_script = new \WeDevs\ERP\Accounting\Admin_Menu();
        $chart_script->chart_script();
        $chart_script->common_scripts();
        wp_enqueue_script( 'erp-ac-reimur', WPERP_REIMBURSEMENT_ASSETS . '/reimbursement.js', array('jquery'), WPERP_REIMBURSEMENT_VERSION, true );
        wp_localize_script( 'erp-ac-reimur', 'erp_ac_tax', [ 'rate' => erp_ac_get_tax_info() ] );
    }

    /**
     * Load reimbursement view page
     *
     * @since 0.1
     *
     * @return void
     */
    function reimursement() {

        $action   = isset( $_GET['action'] ) ? $_GET['action'] : 'list';
        $type     = isset( $_GET['type'] ) ? $_GET['type'] : 'pv';
        $id       = isset( $_GET['id'] ) ? intval( $_GET['id'] ) : 0;
        $draft    = isset( $_GET['transaction_id'] ) ? intval( $_GET['transaction_id'] ) : 0;
        $id       = $id ? $id : $draft;

        $template = '';

        switch ( $action ) {
            case 'new':
                //$transaction = \WeDevs\ERP\Accounting\Model\Transaction::find( $id );

                if ( $type == 'reimbur_invoice' ) {
                    $template = WPERP_REIMBURSEMENT_VIEWS . '/invoice-new.php';
                } else if ( $type == 'reimbur_payment' && ! erp_ac_reimbur_is_employee() ) {
                   $template = WPERP_REIMBURSEMENT_VIEWS . '/payment-new.php';
                }else {
                    $template = apply_filters( 'erp_ac_reimbursement_invoice_transaction_template', $template );
                }

                break;

            case 'view':
                $transaction = \WeDevs\ERP\Accounting\Model\Transaction::find( $id );

                if ( $transaction->form_type == 'reimbur_invoice' ) {
                    $template = WPERP_REIMBURSEMENT_VIEWS . '/invoice-single.php';
                } else {
                    $template = WPERP_REIMBURSEMENT_VIEWS . '/payment-single.php';
                }

                break;

            default:
                $template = WPERP_REIMBURSEMENT_VIEWS . '/reimbursement-list.php';
                break;
        }

        if ( file_exists( $template ) ) {
            include $template;
        } else {
            echo sprintf( '<h1>%s</h1>', __( 'You do not have sufficient permissions to access this page.', 'erp' ) );
        }
    }
}

\WeDevs\ERP\Accounting\Reimbursement\Admin::init();



