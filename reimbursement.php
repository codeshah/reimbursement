<?php
/**
 * Plugin Name: WP ERP - Reimbursement 
 * Description: Reimbursement addon for WP ERP - Accounting module.
 * Plugin URI: https://wperp.com/accounting/reimbursement
 * Author: weDevs
 * Author URI: https://wedevs.com
 * Version: 1.0.0
 * License: GPL2
 * Text Domain: erp-reimbursement
 * Domain Path: languages
 *
 * Copyright (c) 2016 weDevs (email: info@wedevs.com). All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * **********************************************************************
 */

// don't call the file directly
if ( !defined( 'ABSPATH' ) ) exit;


/**
 * Accounting reimbursement plugin main class
 */
class WeDevs_ERP_AC_Reimbursement {
	/**
	 * Version
	 *
	 * @var  string
	 */
	public $version = '1.0.0';

	/**
     * Initializes the class
     *
     * Checks for an existing instance
     * and if it doesn't find one, creates it.
     */
    public static function init() {
        static $instance = false;

        if ( ! $instance ) {
            $instance = new self();
        }

        return $instance;
    }

   	/**
     * Constructor for the class
     *
     * Sets up all the appropriate hooks and actions
     */
    public function __construct() {

        // Localize our plugin
        add_action( 'init', [ $this, 'localization_setup' ] );

        // plugin not installed - notice
        add_action( 'admin_notices', [ $this, 'admin_notice' ] );

        add_action( 'admin_footer', array( $this, 'admin_js_templates' ) );

        // on ERP CRM loaded hook
        add_action( 'erp_accounting_loaded', [ $this, 'erp_accounting_loaded' ] );
    }

    /**
     * Print JS templates in footer
     *
     * @return void
     */
    public function admin_js_templates() {
        global $current_screen;

        if ( $current_screen->base == 'accounting_page_erp-accounting-reimbursement' ) {
            erp_get_js_template( WPERP_ACCOUNTING_JS_TMPL . '/invoice.php', 'erp-ac-invoice-payment-pop' );
        }
    }

    /**
     * Initialize plugin for localization
     *
     * @uses load_plugin_textdomain()
     */
    public function localization_setup() {
        load_plugin_textdomain( 'erp-reimbursement', false, dirname( plugin_basename( __FILE__ ) ) . '/i18n/languages/' );
    }

    /**
     * Display an error message if WP ERP is not active
     *
     * @return void
     */
    public function admin_notice() {
        if ( ! class_exists( 'WeDevs_ERP' ) ) {
            printf(
                '%s'. __( '<strong>Error:</strong> <a href="%s">WP ERP</a> Plugin is required to use Email Campaign plugin.', 'erp-reimbursement' ) . '%s',
                '<div class="message error"><p>',
                'https://wordpress.org/plugins/erp/',
                '</p></div>'
            );
        }
    }

    /**
     * Executes while Plugin Activation
     *
     * @return void
     */
    public static function activate() {
        if ( ! class_exists( 'WeDevs_ERP' ) ) {
            require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
            deactivate_plugins( plugin_basename( __FILE__ ) );
            wp_die( __( 'You need to install WP-ERP main plugin to use this addon', 'erp-reimbursement' ) );
        }
        
        include_once dirname(__FILE__) . '/includes/class-install.php';
    }

    /**
     * Executes if CRM is installed
     *
     * @return boolean|void
     */
    public function erp_accounting_loaded() {
        $this->define_constants();
        $this->includes();
    }

    /**
     * Define Add-on constants
     *
     * @return void
     */
    private function define_constants() {
        $this->define( 'WPERP_REIMBURSEMENT_VERSION', $this->version );
        $this->define( 'WPERP_REIMBURSEMENT_FILE', __FILE__ );
        $this->define( 'WPERP_REIMBURSEMENT_PATH', dirname( WPERP_REIMBURSEMENT_FILE ) );
        $this->define( 'WPERP_REIMBURSEMENT_INCLUDES', WPERP_REIMBURSEMENT_PATH . '/includes' );
        $this->define( 'WPERP_REIMBURSEMENT_URL', plugins_url( '', WPERP_REIMBURSEMENT_FILE ) );
        $this->define( 'WPERP_REIMBURSEMENT_ASSETS', WPERP_REIMBURSEMENT_URL . '/assets' );
        $this->define( 'WPERP_REIMBURSEMENT_VIEWS', WPERP_REIMBURSEMENT_PATH . '/views' );
    }

    /**
     * Define constant if not already set
     *
     * @param  string $name
     * @param  string|bool $value
     * @return type
     */
    private function define( $name, $value ) {
        if ( ! defined( $name ) ) {
            define( $name, $value );
        }
    }

    /**
     * Include required files
     *
     * @return void
     */
    private function includes() {
        include_once WPERP_REIMBURSEMENT_INCLUDES . '/functions-reimburs.php';
        include_once WPERP_REIMBURSEMENT_INCLUDES . '/class-transaction-list-table.php';
        include_once WPERP_REIMBURSEMENT_INCLUDES . '/class-admin.php';
    }

}

// on plugin register hook
register_activation_hook( __FILE__, [ 'WeDevs_ERP_AC_Reimbursement', 'activate' ] );


WeDevs_ERP_AC_Reimbursement::init();



