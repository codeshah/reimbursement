<?php
namespace WeDevs\ERP\Accounting\Reimbursement;

if ( ! class_exists ( 'WP_List_Table' ) ) {
    require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}

/**
 * List table class
 */
class Reimbursement_Transaction_List_Table extends \WeDevs\ERP\Accounting\Transaction_List_Table {
    private $counts = array();
    private $page_status = '';

    function __construct() {
        global $status, $page;

        $this->type = 'reimbur';
        $this->slug = 'erp-accounting-reimbursement';

        \WP_List_Table::__construct([
            'singular' => 'reimbursement',
            'plural'   => 'reimbursements',
            'ajax'     => false
        ]);
    }

    /**
     * Generate the table navigation above or below the table
     *
     * @since 3.1.0
     * @access protected
     * @param string $which
     */
    protected function display_tablenav( $which ) {
        if ( 'top' === $which ) {
            wp_nonce_field( 'bulk-' . $this->_args['plural'] );
        }
        ?>
        <div class="tablenav <?php echo esc_attr( $which ); ?>">
        <?php
            $this->extra_tablenav( $which );
            $this->pagination( $which );
        ?>

        <br class="clear" />
        </div>
    <?php
    }

    /**
     * Get form types
     *
     * @return array
     */
    public function get_form_types() {
        return erp_ac_reimbursement_register_form_types();
    }

    /**
     * Get the column names
     *
     * @return array
     */
    function get_columns() {
        $section = isset( $_GET['section'] ) ? $_GET['section'] : false;
        $columns = array(
            //'cb'         => '<input type="checkbox" />',
            'issue_date' => __( 'Date', 'erp' ),
            //'form_type'  => __( 'Type', 'erp' ),
            'user_id'    => __( 'Receipt From', 'erp' ),
            'ref'        => __( 'Ref', 'erp' ),
            'due'        => __( 'Due', 'erp' ),
            'total'      => __( 'Total', 'erp' ),
            'status'     => __( 'Status', 'erp' ),
        );

       if (  ( ! erp_ac_reimbur_is_employee() && ( $section == 'awaiting-approval' || $section == 'void' || $section == 'closed' || $section == 'awaiting-payment' ) || $section == 'draft' ) ) {
            $action = [ 'cb' => '<input type="checkbox" />'];
            $columns = array_merge( $action, $columns );
       }

       if ( erp_ac_reimbur_is_employee() && $section == 'draft' ) {
            $action = [ 'cb' => '<input type="checkbox" />'];
            $columns = array_merge( $action, $columns );
       }

        return $columns;
    }

    /**
     * Render the issue date column
     *
     * @param  object  $item
     *
     * @return string
     */
    function column_issue_date( $item ) {
        $section  = isset( $_GET['section'] ) ? $_GET['section'] : '';
        $url      = admin_url( 'admin.php?page='.$this->slug.'&action=new&type=' . $item->form_type . '&transaction_id=' . $item->id .'&section=' . $section );
        $paid_url = admin_url( 'admin.php?page=erp-accounting-reimbursement&action=new&type=reimbur_payment&transaction_id=' . $item->id );
        $edit     = sprintf( '<a href="%1s">%2s</a>', $url, __( 'Edit', 'erp' ) );
        $delete   = sprintf( '<a href="#" class="erp-ac-reimbur-trns-row-status" data-status="delete" data-id="%d" title="%s">%s</a>', $item->id, __( 'Delete', 'erp' ), __( 'Delete', 'erp' ) );
        $void     = sprintf( '<a href="#" class="erp-ac-reimbur-trns-row-status" data-status="void" data-id="%d" title="%s">%s</a>', $item->id, __( 'Void', 'erp' ), __( 'Void', 'erp' ) );
        $approve  = sprintf( '<a href="#" class="erp-ac-reimbur-trns-row-status" data-status="awaiting_approval" data-id="%d" title="%s">%s</a>', $item->id, __( 'Request Approval', 'erp' ), __( 'Request Approval', 'erp' ) );
        $payment  = sprintf( '<a href="#" class="erp-ac-reimbur-trns-row-status" data-status="awaiting_payment" data-id="%d" title="%s">%s</a>', $item->id, __( 'Request Approval', 'erp' ), __( 'Request Payment', 'erp' ) );
        $paid     = sprintf( '<a href="%s"  title="%s">%s</a>', $paid_url, __( 'Pay Now', 'erp' ), __( 'Pay Now', 'erp' ) );

        if ( $item->status == 'draft' ) {
            $actions['approval'] =  $approve;
        }

        if ( $item->status == 'awaiting_approval' && ! erp_ac_reimbur_is_employee() ) {
             $actions['payment'] =  $payment;
        }

        if ( ( $item->status == 'awaiting_payment' ||  $item->status == 'partial' ) && $item->form_type == 'reimbur_invoice' && ! erp_ac_reimbur_is_employee() ) {
            $actions['paid'] = $paid;
        }

        if ( ( $item->status == 'awaiting_approval' || $item->status == 'awaiting_payment' || $item->status == 'closed' || $item->status == 'partial' || $item->status == 'paid' ) && ! erp_ac_reimbur_is_employee() ) {
            $actions['void'] = $void;
        }

        if ( ($item->status == 'draft' || ( $item->status == 'awaiting_payment' || $item->status == 'awaiting_approval' ) && ! erp_ac_reimbur_is_employee() ) ) {
            $actions['edit'] = $edit;
        }

        if ( $item->status == 'draft' || ( $item->status == 'void' && ! erp_ac_reimbur_is_employee() ) ) {
            $actions['delete'] = $delete;
        }

        if ( isset( $actions ) && count( $actions ) ) {
            return sprintf( '<a href="%1$s">%2$s</a> %3$s', admin_url( 'admin.php?page=' . $this->slug . '&action=view&id=' . $item->id ), erp_format_date( $item->issue_date ), $this->row_actions( $actions ) );
        } else {
            return sprintf( '<a href="%1$s">%2$s</a>', admin_url( 'admin.php?page=' . $this->slug . '&action=view&id=' . $item->id ), erp_format_date( $item->issue_date ) );
        }
    }

    /**
     * Status
     *
     * @param  array $item
     *
     * @return string
     */
    public function column_status( $item ) {

        switch ( $item->status ) {
            case 'awaiting_approval':
                if (  erp_ac_reimbur_is_employee() ) {
                    $status = __( 'Awaiting Approval', 'erp-reimbursement' );
                    break;
                }

                $url   = admin_url( 'admin.php?page='.$this->slug.'&action=new&type=' . $item->form_type . '&transaction_id=' . $item->id );
                $status = sprintf( '<a href="%1s">%2s</a>', $url, __( 'Awaiting Approval', 'erp-reimbursement' ) );
                break;
            case 'draft':
                $url   = admin_url( 'admin.php?page='.$this->slug.'&action=new&type=' . $item->form_type . '&transaction_id=' . $item->id );
                $status = sprintf( '<a href="%1s">%2s</a>', $url, __( 'Draft', 'erp-reimbursement' ) );
                break;

            case 'awaiting_payment':
                if (  erp_ac_reimbur_is_employee() ) {
                    $status = __( 'Awaiting Payment', 'erp-reimbursement' );
                    break;
                }

                $url   = admin_url( 'admin.php?page='.$this->slug.'&action=new&type=reimbur_payment&transaction_id=' . $item->id );
                $status = sprintf( '<a href="%1s">%2s</a>', $url, __( 'Awaiting Payment', 'erp-reimbursement' ) );
                break;

            case 'paid':
                $url   = admin_url( 'admin.php?page='.$this->slug.'&action=new&type=reimbur_payment&transaction_id=' . $item->id );
                $status = __( 'Paid', 'erp-reimbursement' );
                break;

            case 'closed':
                $url   = admin_url( 'admin.php?page='.$this->slug.'&action=new&type=reimbur_payment&transaction_id=' . $item->id );
                $status = __( 'Closed', 'erp-reimbursement' );
                break;


            case 'void':
                $url   = admin_url( 'admin.php?page='.$this->slug.'&action=new&type=' . $item->form_type . '&transaction_id=' . $item->id );
                $status = sprintf( '<a href="%1s">%2s</a>', $url, __( 'Void', 'erp-reimbursement' ) );
                break;

            case 'partial':
                if ( erp_ac_reimbur_is_employee() ) {
                    $status = __( 'Partially Paid', 'erp-reimbursement' );
                } else {
                    $url = admin_url( 'admin.php?page=erp-accounting-reimbursement&action=new&type=reimbur_payment&transaction_id=' . $item->id );
                    $status = sprintf( '<a href="%1s">%2s</a>', $url, __( 'Partially Paid', 'erp-reimbursement' ) );
                }
                break;
        }

        return isset( $status ) ? $status : '';
    }

    public function column_form_type( $item ) {
        if ( $item->form_type == 'reimbur_payment' ) {
            return __( 'Payment', 'erp-reimbursement' );
        }

        return __( 'Invoice', 'erp-reimbursement' );
    }

    public function column_user_id( $item ) {


        $user_display_name = '';
        $actions           = array();
        $transaction       = \WeDevs\ERP\Accounting\Model\Transaction::find( $item->id );

        $employee = get_user_by( 'id', intval( $transaction->user_id ) );
        if ( erp_ac_reimbursement_is_hrm_active() && user_can($transaction->user_id, 'employee') ) {
            $employee          = new \WeDevs\ERP\HRM\Employee( intval($transaction->user_id) );
            $user_display_name = $employee->get_full_name();
            $profile           = $employee->get_details_url();

        } else {
            $user_display_name = $employee->display_name;
            $profile           = admin_url( 'user-edit.php?user_id=' . $transaction->user_id );
        }

        return sprintf( '<a href="%1$s">%2$s</a> %3$s', $profile, $user_display_name, $this->row_actions( $actions ) );
    }

    /**
     * Filters
     *
     * @param  string  $which
     *
     * @return void
     */
    public function extra_tablenav( $which ) {
        $section = isset( $_GET['section'] ) ? $_GET['section'] : false;
        if ( 'top' == $which ) {
            echo '<div class="alignleft actions">';

            $all_types = $this->get_form_types();
            $types = [];

            foreach ($all_types as $key => $type) {
                $types[ $key ] = $type['label'];
            }

            $type = [];

            if ( $section == 'void' ) {
                $type = ['delete' => __( 'Delete', 'erp-reimbursement' )];
            } else if ( $section == 'awaiting-approval' ) {
                $type = ['void' => __( 'Void', 'erp-reimbursment' ), 'awaiting_payment' => __( 'Pay Now', 'erp-reimbursment' )];
            } else if ( $section == 'awaiting-payment' || $section == 'closed' ) {
                $type = ['void' => __( 'Void', 'erp-reimbursment' )];
            } else if ( $section == 'draft' ) {
                $type = ['delete' => __( 'Delete', 'erp-reimbursement' ), 'awaiting_approval' => __( 'Approve', 'erp-reimbursement' )];
            }

            if ( $section ) {
                erp_html_form_input([
                    'name'        => 'action',
                    'type'        => 'select',
                    'options'      => [ '-1' => __( 'Bulk Actions', 'erp-reimbursement' ) ] + $type
                ]);

                submit_button( __( 'Apply', 'erp-reimbursement' ), 'button', 'submit_action_delete', false, ['data-status' => $section] );
            }


            erp_html_form_input([
                'name'        => 'user_id',
                'type'        => 'hidden',
                'class'       => 'erp-ac-customer-search',
                'placeholder' => __( 'Search for Customer', 'erp' ),
            ]);

            erp_html_form_input([
                'name'        => 'start_date',
                'class'       => 'erp-date-field',
                'value'       => isset( $_REQUEST['start_date'] ) && !empty( $_REQUEST['start_date'] ) ? $_REQUEST['start_date'] : '',
                'placeholder' => __( 'Start Date', 'erp' )
            ]);

            erp_html_form_input([
                'name'        => 'end_date',
                'class'       => 'erp-date-field',
                'value'       => isset( $_REQUEST['end_date'] ) && !empty( $_REQUEST['end_date'] ) ? $_REQUEST['end_date'] : '',
                'placeholder' => __( 'End Date', 'erp' )
            ]);

            erp_html_form_input([
                'name'        => 'ref',
                'value'       => isset( $_REQUEST['ref'] ) && ! empty( $_REQUEST['ref'] ) ? $_REQUEST['ref'] : '',
                'placeholder' => __( 'Ref No.', 'erp' )
            ]);

            submit_button( __( 'Filter', 'erp' ), 'button', 'submit_filter_sales', false );

            echo '</div>';
        }
    }

    /**
     * Set the views
     *
     * @return array
     */
    public function get_views() {
        $status_links   = array();
        $base_link      = admin_url( 'admin.php?page=erp-hr-employee' );

        foreach ( $this->counts as $key => $value ) {
            $class = ( $key == $this->page_status ) ? 'current' : 'status-' . $key;
            $status_links[ $key ] = sprintf( '<a href="%s" class="%s">%s <span class="count">(%s)</span></a>', erp_ac_reimbur_section_url( $key ), $class, $value['label'], $value['count'] );
        }

        return $status_links;
    }

    /**
     * get section transaction total for pagination
     *
     * @since  0.1
     *
     * @param  array $count
     *
     * @return int
     */
    function get_transaction_count( $count ) {
        $section = isset( $_REQUEST['section'] ) ? $_REQUEST['section'] : '';

        switch ( $section ) {
            case 'draft':
                return isset( $count['draft']['count'] ) ? intval( $count['draft']['count'] ) : 0;
                break;

            case 'awaiting-approval':
                return isset( $count['awaiting_approval']['count'] ) ? intval( $count['awaiting_approval']['count'] ) : 0;
                break;

            case 'awaiting-payment':
                return isset( $count['awaiting_payment']['count'] ) ? intval( $count['awaiting_payment']['count'] ) : 0;
                break;

            case 'paid':
                return isset( $count['paid']['count'] ) ? intval( $count['paid']['count'] ) : 0;
                break;

            case 'void':
                return isset( $count['void']['count'] ) ? intval( $count['void']['count'] ) : 0;
                break;
            case 'closed':
                return isset( $count['closed']['count'] ) ? intval( $count['closed']['count'] ) : 0;
                break;


            case 'partial':
                return isset( $count['partial']['count'] ) ? intval( $count['partial']['count'] ) : 0;
                break;

            default:
                return isset( $count['all']['count'] ) ? intval( $count['all']['count'] ) : 0;
                break;
        }
    }

    /**
     * Prepare the class items
     *
     * @return void
     */
    function prepare_items() {

        $columns               = $this->get_columns();
        $hidden                = array( );
        $sortable              = $this->get_sortable_columns();
        $this->_column_headers = array( $columns, $hidden, $sortable );

        $per_page              = 20;
        $current_page          = $this->get_pagenum();
        $offset                = ( $current_page -1 ) * $per_page;
        $this->page_status     = isset( $_GET['section'] ) ? erp_ac_reimbur_get_status_from_url( sanitize_text_field( $_GET['section'] ) ) : 'all';

        // only ncessary because we have sample data
        $args = array(
            'type'   => $this->type,
            'offset' => $offset,
            'number' => $per_page,
            //'form_type' => 'reimbur_invoice',
        );

        if ( isset( $_REQUEST['orderby'] ) && isset( $_REQUEST['order'] ) ) {
            $args['orderby'] = $_REQUEST['orderby'];
            $args['order']   = $_REQUEST['order'] ;
        }

        // search params
        if ( isset( $_REQUEST['start_date'] ) && !empty( $_REQUEST['start_date'] ) ) {
            $args['start_date'] = $_REQUEST['start_date'];
        }

        if ( isset( $_REQUEST['end_date'] ) && !empty( $_REQUEST['end_date'] ) ) {
            $args['end_date'] = $_REQUEST['end_date'];
        }

        if ( isset( $_REQUEST['form_type'] ) && ! empty( $_REQUEST['form_type'] ) ) {
           $args['form_type'] = $_REQUEST['form_type'];
        }

        if ( isset( $_REQUEST['section'] ) ) {
            $args['status'] = erp_ac_reimbur_get_status_from_url( $_REQUEST['section'] );
        }

        if ( isset( $_REQUEST['ref'] ) && ! empty( $_REQUEST['ref'] ) ) {
            $args['ref'] = $_REQUEST['ref'];
        }

        if ( erp_ac_reimbur_is_employee() ) {
            $args['user_id'] = get_current_user_id();
        }
        $this->counts = erp_ac_reimbur_transaction_count();
        $this->items  = $this->get_transactions( $args );

        $this->set_pagination_args( array(
            'total_items' => $this->get_transaction_count( $this->counts ),
            'per_page'    => $per_page
        ) );
    }
}