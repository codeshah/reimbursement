<?php $reimburs = erp_ac_reimbursement_register_form_types(); ?>
<div class="wrap erp-accounting">

    <h2>
        <?php 
        _e( 'Reimbursements', 'erp-reimbursement' ); 
        
        foreach ( $reimburs as $key => $reimbur ) {
            printf( '<a class="add-new-h2" href="%s%s" title="%s">%s</a> ', 
                admin_url( 'admin.php?page=erp-accounting-reimbursement&action=new&type=' ), 
                $key, 
                esc_attr( $reimbur['description'] ), 
                $reimbur['label'] );
        } 
        ?>
    </h2>

    <form method="get">
        <input type="hidden" name="page" value="erp-accounting-reimbursement">

        <?php
        $list_table = new WeDevs\ERP\Accounting\Reimbursement\Reimbursement_Transaction_List_Table();
        $list_table->prepare_items();
        $list_table->views();

        $list_table->display();
        ?>
    </form>

</div>

